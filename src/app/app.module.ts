import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MyFistComponentComponent } from './my-fist-component/my-fist-component.component';
import { MySecondComponentComponent } from './my-second-component/my-second-component.component';
import { MyOwnComponent } from './my-own-component/my-own-component.component';
import { AppRoutingModule } from './app-routing.module';

import { TodoModule } from './todo-module/todo.module';

@NgModule({
  declarations: [
    AppComponent,
    MyFistComponentComponent,
    MySecondComponentComponent,
    MyOwnComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // przy stworzeniu nowego modułu dodajemy go do importów
    TodoModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
