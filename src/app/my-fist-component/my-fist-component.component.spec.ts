import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyFistComponentComponent } from './my-fist-component.component';

describe('MyFistComponentComponent', () => {
  let component: MyFistComponentComponent;
  let fixture: ComponentFixture<MyFistComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyFistComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyFistComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
