import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoItemComponent } from './components/todo-item/todo-item.component';
import { TodoModuleComponent } from './todo-module.component';
import { TodoService } from './services/todo.service';

// moduł odpowiedzialny za Todo
@NgModule({
  declarations: [
    TodoListComponent,
    TodoItemComponent,
    TodoModuleComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    //aby móc używać tych komponentów w innych modułach, należy wyeksportowaćje
    TodoListComponent,
    TodoItemComponent,
    TodoModuleComponent
  ],
  providers: [
    TodoService
  ]
})
export class TodoModule { }
