import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TodoInterface } from '../../interfaces/todo-item.interface';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent implements OnInit {
  @Input() todo: TodoInterface;
  @Output() isDone = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  changeStatusClick () {
    this.isDone.emit(!this.todo.isDone);
  }
}
