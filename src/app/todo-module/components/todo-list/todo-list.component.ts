import { Component, OnInit } from '@angular/core';
import { TODO_LIST } from '../../consts/todo-list.const';
import { TodoService } from '../../services/todo.service';
import { TodoInterface } from '../../interfaces/todo-item.interface';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})

export class TodoListComponent implements OnInit {
  // w tym zadaniu będziemy używać zwykłego wyciągania danych z consta
  todoList = TODO_LIST;
  

  // zaawansowanym sposobem jest wyciąganie danych z servisu i zapisywanie tych danych dozmiennej
  todoListFromService: TodoInterface[] = [];

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
    //wyciągnięcie danych z servisu
    this.todoService.getTodos()
                    .then(todos => (this.todoListFromService = todos));
  }

  changeStatusToParentClick (isTodoDone: boolean, index: number): void {
    this.todoList[index].isDone = isTodoDone;
    if (isTodoDone) {
      this.todoList[index].status = 'Done';
    } else {
      this.todoList[index].status = 'In progress';
    }
  }
}


