export interface TodoInterface {
    name: string,
    dateAdded: string,
    status: string,
    isDone: boolean
}