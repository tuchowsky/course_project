import { Injectable } from '@angular/core';
import { TodoInterface } from '../interfaces/todo-item.interface';
import { TODO_LIST } from '../consts/todo-list.const';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  constructor() { }

  getTodos(): Promise<TodoInterface[]> {
    return Promise.resolve(TODO_LIST);
  }
  
}
