import { TodoInterface } from '../interfaces/todo-item.interface';

export const TODO_LIST: TodoInterface[] = [
    {
        name: 'Dodać możliwość dodawana pojedynczego Todo',
        dateAdded: '22/08/2020',
        status: 'In progress',
        isDone: false
    },
    {
        name: 'Dodać możliwość usuwania pojedynczego Todo',
        dateAdded: '22/08/2020',
        status: 'In progress',
        isDone: false
    },
    {
        name: 'Dodać możliwość edycji pojedynczego Todo',
        dateAdded: '22/08/2020',
        status: 'In progress',
        isDone: false
    },
    {
        name: 'Dodać dodać automatyczne generowanie daty utworzenia Todo, przy dodawaniu Todo',
        dateAdded: '22/08/2020',
        status: 'In progress',
        isDone: false
    },
    {
        name: 'Todo name 3',
        dateAdded: '22/08/2020',
        status: 'Done',
        isDone: true
    },
    {
        name: 'Todo name 4',
        dateAdded: '22/08/2020',
        status: 'Done',
        isDone: true
    }
];