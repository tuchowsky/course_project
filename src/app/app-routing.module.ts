import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoModuleComponent } from './todo-module/todo-module.component';

const routes: Routes = [
  {path: 'todo', component: TodoModuleComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
